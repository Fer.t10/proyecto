document.addEventListener("DOMContentLoaded", function() {
    var presentacionDiv = document.getElementById("presentacion");

    // Contenido de la presentación del grupo
    var presentacionContenido = "<h2>Nuestro Grupo</h2>" +
                                "<p>Somos un equipo apasionado de personas trabajando juntas para lograr grandes cosas. Aquí hay algunas cosas sobre nosotros:</p>" +
                                "<ul>" +
                                "<li>Amamos aprender y crecer juntos.</li>" +
                                "<li>Nos encanta colaborar en proyectos innovadores.</li>" +
                                "<li>Tenemos diversidad de talentos y habilidades.</li>" +
                                "</ul>";

    // Agrega el contenido al div de presentación
    presentacionDiv.innerHTML = presentacionContenido;
});